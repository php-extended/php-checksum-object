<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

use Iterator;
use IteratorIterator;

/**
 * ChecksumIterator class file.
 * 
 * This iterator transforms all the payloads into checksums and keeps the keys
 * the same.
 * 
 * @author Anastaszor
 * @extends \IteratorIterator<integer, string, \Iterator<integer, ?string>>
 * @psalm-suppress InvalidTemplateParam
 * @phpstan-ignore-next-line
 */
class ChecksumIterator extends IteratorIterator
{
	
	/**
	 * The checksum.
	 * 
	 * @var ChecksumInterface
	 */
	protected ChecksumInterface $_checksum;
	
	/**
	 * Builds a new ChecksumIterator with its dependancies.
	 * 
	 * @param ChecksumInterface $checksum
	 * @param Iterator<integer, ?string> $inner
	 */
	public function __construct(ChecksumInterface $checksum, Iterator $inner)
	{
		parent::__construct($inner);
		$this->_checksum = $checksum;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorIterator::current()
	 */
	public function current() : string
	{
		return $this->_checksum->calculate((string) parent::current());
	}
	
}
