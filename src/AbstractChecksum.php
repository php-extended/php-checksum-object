<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

use Iterator;

/**
 * AbstractChecksum class file.
 * 
 * This class gathers all code that is common between all algorithms.
 * 
 * @author Anastaszor
 */
abstract class AbstractChecksum implements ChecksumInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculateOverAll()
	 */
	public function calculateOverAll(array $data) : string
	{
		$gathered = '';
		
		foreach($data as $value)
		{
			$gathered .= (string) $value;
		}
		
		return $this->calculate($gathered);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculateIterator()
	 */
	public function calculateOverIterator(Iterator $iterator) : string
	{
		$data = '';
		
		foreach($iterator as $value)
		{
			$data .= (string) $value;
		}
		
		return $this->calculate($data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculateAll()
	 */
	public function calculateAll(array $data) : array
	{
		$result = [];
		
		foreach($data as $datum)
		{
			$result[] = $this->calculate($datum);
		}
		
		return $result;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculateIterator()
	 */
	public function calculateIterator(Iterator $iterator) : Iterator
	{
		return new ChecksumIterator($this, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::matches()
	 */
	public function matches(string $checksum, ?string $data) : bool
	{
		return 0 === \strcmp($checksum, $this->calculate($data));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::matchesOverAll()
	 */
	public function matchesOverAll(string $checksum, array $data) : bool
	{
		return 0 === \strcmp($checksum, $this->calculateOverAll($data));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::matchesIterator()
	 */
	public function matchesOverIterator(string $checksum, Iterator $iterator) : bool
	{
		return 0 === \strcmp($checksum, $this->calculateOverIterator($iterator));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::filterAllMatching()
	 */
	public function filterAllMatching(array $data) : array
	{
		$filtered = [];
		
		foreach($data as $value => $checksum)
		{
			if($this->matches($checksum, $value))
			{
				$filtered[$value] = $checksum;
			}
		}
		
		return $filtered;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::filterIteratorAllMatching()
	 */
	public function filterIteratorAllMatching(Iterator $iterator) : Iterator
	{
		return new MatchingIterator($this, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::filterNoneMatching()
	 */
	public function filterNoneMatching(array $data) : array
	{
		$filtered = [];
		
		foreach($data as $value => $checksum)
		{
			if(!$this->matches($checksum, $value))
			{
				$filtered[$value] = $checksum;
			}
		}
		
		return $filtered;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::filterIteratorNoneMatching()
	 */
	public function filterIteratorNoneMatching(Iterator $iterator) : Iterator
	{
		return new MismatchingIterator($this, $iterator);
	}
	
}
