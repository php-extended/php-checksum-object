<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

/**
 * ChecksumLuhn class file.
 * 
 * This class implements the checksum interface for the luhn algorithm.
 * 
 * @author Anastaszor
 */
class ChecksumLuhn extends AbstractChecksum
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculate()
	 */
	public function calculate(?string $data) : string
	{
		$sum = 0;
		$even = true;
		$data = (string) $data;
		
		$len = (int) \strlen($data);
		
		for($digit = $len - 1; 0 <= $digit; $digit--)
		{
			$num = \ord($data[$digit]) - 48; // 48 == ord('0');
			
			// must be only one digit
			$num %= 10;
			
			// must be positive
			while(0 > $num)
			{
				$num += 10;
			}
			
			if($even)
			{
				$num *= 2;
				$num = ($num % 10) + (int) ($num / 10);
			}
			
			$even = !$even;
			$sum += $num;
		}
		
		return (string) ((10 - ($sum % 10)) % 10);
	}
	
}
