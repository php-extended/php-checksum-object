<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

/**
 * ChecksumMd5 class file.
 * 
 * This class calculates checksums with the md5 algorithm.
 * 
 * @author Anastaszor
 */
class ChecksumMd5 extends AbstractChecksum
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculate()
	 */
	public function calculate(?string $data) : string
	{
		return \md5((string) $data);
	}
	
}
