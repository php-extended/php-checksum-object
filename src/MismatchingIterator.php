<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

use FilterIterator;
use Iterator;

/**
 * MismatchingIterator class file.
 *
 * This iterator accepts only values that DO NOT matches their checksum.
 *
 * @author Anastaszor
 * @extends \FilterIterator<string, string, \Iterator<string, string>>
 */
class MismatchingIterator extends FilterIterator
{
	
	/**
	 * The checksum to match against.
	 * 
	 * @var ChecksumInterface
	 */
	protected ChecksumInterface $_checksum;
	
	/**
	 * Builds a new MismatchingIterator with the given checksum.
	 * 
	 * @param ChecksumInterface $checksum
	 * @param Iterator<string, string> $inner
	 */
	public function __construct(ChecksumInterface $checksum, Iterator $inner)
	{
		parent::__construct($inner);
		$this->_checksum = $checksum;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \FilterIterator::accept()
	 */
	public function accept() : bool
	{
		$checksum = (string) parent::current();
		$data = (string) parent::key();
		
		return !$this->_checksum->matches($checksum, $data);
	}
	
}
