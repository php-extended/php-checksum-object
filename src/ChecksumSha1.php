<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Checksum;

/**
 * ChecksumSha1 class file.
 * 
 * This calculates checksums with the sha1 algorithm.
 * 
 * @author Anastaszor
 */
class ChecksumSha1 extends AbstractChecksum
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Checksum\ChecksumInterface::calculate()
	 */
	public function calculate(?string $data) : string
	{
		return \sha1((string) $data);
	}
	
}
