# php-extended/php-checksum-object

A library that implements the php-extended/php-checksum-interface for the object algorithm.

![coverage](https://gitlab.com/php-extended/php-checksum-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-checksum-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-checksum-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\Checksum\Checksumobject;

$checksum = new ChecksumLuhn();

$data = '<put your digit string here>';
$checked = $checksum->matches(substr($data, 0, -1), substr($data, -1));
// returns true if verified

$signed = $data.$checksum->calculate($data);
// appends the calculated digit to the checksum

```


## License

MIT (See [license file](LICENSE)).
