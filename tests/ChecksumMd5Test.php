<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Checksum\ChecksumMd5;
use PHPUnit\Framework\TestCase;

/**
 * ChecksumMd5Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Checksum\ChecksumMd5
 * @internal
 * @small
 */
class ChecksumMd5Test extends TestCase
{
	
	public static function provideCalculate() : iterable
	{
		return [
			['', 'd41d8cd98f00b204e9800998ecf8427e'],
			['a', '0cc175b9c0f1b6a831c399e269772661'],
			['abc', '900150983cd24fb0d6963f7d28e17f72'],
			['message digest', 'f96b697d7cb7938d525a2f31aaf161d0'],
			['abcdefghijklmnopqrstuvwxyz', 'c3fcd3d76192e4007dfb496cca67e13b'],
			['ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 'd174ab98d277d9f5a5611c2c9f419d9f'],
			['12345678901234567890123456789012345678901234567890123456789012345678901234567890', '57edf4a22be3c955ac49da2e2107b67a'],
			['你好吗？', 'bb0b6bc45375143826f72439e050743e'],
			['お元気ですか', '2e0bd9e58d042f5234a1202cc3c7d499'],
		];
	}
	
	public static function provideCalculateIterator() : iterable
	{
		return [
			[new ArrayIterator(['abcdefghijklm', 'nopqrstuvwxyz']), 'c3fcd3d76192e4007dfb496cca67e13b'],
			[new ArrayIterator(['ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz', '0123456789']), 'd174ab98d277d9f5a5611c2c9f419d9f'],
			[new ArrayIterator(['1234567890123456789', '01234567890123456789', '012345678901234567890', '12345678901234567890']), '57edf4a22be3c955ac49da2e2107b67a'],
			[new ArrayIterator(['你好', '吗？']), 'bb0b6bc45375143826f72439e050743e'],
			[new ArrayIterator(['お元気', 'ですか']), '2e0bd9e58d042f5234a1202cc3c7d499'],
		];
	}
	
	/**
	 * The object, test.
	 * 
	 * @var ChecksumMd5
	 */
	protected ChecksumMd5 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * @dataProvider provideCalculate
	 * @param string $data
	 * @param string $checksum
	 */
	public function testCalculate(string $data, string $checksum) : void
	{
		$this->assertSame($checksum, $this->_object->calculate($data));
	}
	
	/**
	 * @dataProvider provideCalculateIterator
	 * @param Iterator $data
	 * @param string $checksum
	 */
	public function testCalculateIterator(Iterator $data, string $checksum) : void
	{
		$this->assertSame($checksum, $this->_object->calculateOverIterator($data));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ChecksumMd5();
	}
	
}
