<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Checksum\ChecksumSha1;
use PHPUnit\Framework\TestCase;

/**
 * ChecksumSha1Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Checksum\ChecksumSha1
 * @internal
 * @small
 */
class ChecksumSha1Test extends TestCase
{
	
	public static function provideCalculate() : iterable
	{
		return [
			['', 'da39a3ee5e6b4b0d3255bfef95601890afd80709'],
			['The quick brown fox jumps over the lazy dog', '2fd4e1c67a2d28fced849ee1bb76e7391b93eb12'],
			['The quick brown fox jumps over the lazy cog', 'de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3'],
		];
	}
	
	public static function provideCalculateIterator() : iterable
	{
		return [
			[new ArrayIterator(['The quick brown fox ', 'jumps over the lazy dog']), '2fd4e1c67a2d28fced849ee1bb76e7391b93eb12'],
			[new ArrayIterator(['The quick brown fox jumps ', 'over the lazy cog']), 'de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3'],
		];
	}
	
	/**
	 * The object to test.
	 * 
	 * @var ChecksumSha1
	 */
	protected ChecksumSha1 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * @dataProvider provideCalculate
	 * @param string $data
	 * @param string $checksum
	 */
	public function testCalculate(string $data, string $checksum) : void
	{
		$this->assertSame($checksum, $this->_object->calculate($data));
	}
	
	/**
	 * @dataProvider provideCalculateIterator
	 * @param Iterator $data
	 * @param string $checksum
	 */
	public function testCalculateIterator(Iterator $data, string $checksum) : void
	{
		$this->assertSame($checksum, $this->_object->calculateOverIterator($data));
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ChecksumSha1();
	}
	
}
