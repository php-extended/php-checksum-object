<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Checksum\ChecksumMd5;
use PhpExtended\Checksum\MismatchingIterator;
use PHPUnit\Framework\TestCase;

/**
 * MismatchingIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Checksum\MismatchingIterator
 * @internal
 * @small
 */
class MismatchingIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MismatchingIterator
	 */
	protected MismatchingIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$result = \iterator_to_array($this->_object);
		
		$this->assertCount(1, $result);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MismatchingIterator(new ChecksumMd5(), new ArrayIterator(['' => 'd41d8cd98f00b204e9800998ecf8427eeeeeeeeeeeeeeeee']));
	}
	
}
