<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Checksum\ChecksumIterator;
use PhpExtended\Checksum\ChecksumMd5;
use PHPUnit\Framework\TestCase;

/**
 * ChecksumIteratorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Checksum\ChecksumIterator
 * @internal
 * @small
 */
class ChecksumIteratorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ChecksumIterator
	 */
	protected ChecksumIterator $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		$expected = ['d41d8cd98f00b204e9800998ecf8427e'];
		
		$this->assertEquals($expected, \iterator_to_array($this->_object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ChecksumIterator(new ChecksumMd5(), new ArrayIterator(['']));
	}
	
}
