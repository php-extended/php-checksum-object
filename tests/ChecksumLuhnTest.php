<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-checksum-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Checksum\ChecksumLuhn;
use PHPUnit\Framework\TestCase;

/**
 * ChecksumLuhnTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Checksum\AbstractChecksum
 * @covers \PhpExtended\Checksum\ChecksumLuhn
 * @internal
 * @small
 */
class ChecksumLuhnTest extends TestCase
{
	
	/**
	 * @var ChecksumLuhn
	 */
	protected ChecksumLuhn $_luhn;
	
	public function testToString() : void
	{
		$object = $this->_luhn;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testCalculateEmpty() : void
	{
		$this->assertEquals('0', $this->_luhn->calculate(null));
	}
	
	public function testCalculate() : void
	{
		$this->assertEquals('3', $this->_luhn->calculate('7992739871'));
	}
	
	public function testCalculateSpace() : void
	{
		$this->assertEquals('2', $this->_luhn->calculate(' '));
	}
	
	public function testMatchesEmpty() : void
	{
		$this->assertTrue($this->_luhn->matches('0', null));
	}
	
	public function testMatchesNonChecksum() : void
	{
		$this->assertFalse($this->_luhn->matches('00', '0987'));
	}
	
	public function testMatchesLetterChecksum() : void
	{
		$this->assertFalse($this->_luhn->matches('a', '0987'));
	}
	
	public function testLuhn01() : void
	{
		$this->assertTrue($this->_luhn->matches('3', '7992739871'));
	}
	
	public function testLuhn02() : void
	{
		$this->assertTrue($this->_luhn->matches('1', '896101950123440000'));
	}
	
	public function testMatchesIterator() : void
	{
		$this->assertTrue($this->_luhn->matchesOverIterator('1', new ArrayIterator(['896', '101', '950', '123', '440', '000'])));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_luhn = new ChecksumLuhn();
	}
	
}
